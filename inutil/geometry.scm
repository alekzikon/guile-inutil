(define-module (inutil geometry)
  #:export (area-rhombus
            area-trapezoid))


(define (area-rhombus a b)
  "Return the area of a rhombus where A and B are the lengths of its
   diagonals."
  (* 1/2 a b))


(define (area-trapezoid a b h)
  "Return the area of a trapezoid where A and B are the parallel sides
   and H is the distance (height) between the parallels."
  (/ (* (+ a b) h) 2))
