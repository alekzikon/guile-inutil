(define-module (inutil greetings)
  #:export (list-greetings))


(define* (list-greetings #:key (lang "en"))
  "Return a list of greetings in the given language."
  (cond [(string=? lang "en") (list "Hello" "Hi" "Hey")]
        [(string=? lang "es") (list "Hola" "Buenas" "Ey")]
        [else (list)]))
