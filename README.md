# guile-inutil

This library is useless. It is just an example library I use to learn
how to package [Guile][1] libraries for the [Guix System][2].

-----

**NOTE:** The following instructions assume you are using the Guix
System (I don't know anything about guix on foreign operating
systems).

-----


## Installation

1. Add my [my experimental Guix channel][3] to your
   `~/.config/guix/channels.scm`.
2. Install `guile-inutil`:

   ```
   $ guix install guile-inutil
   ```


## Usage

Once installed, you can try using any of the modules of the library
in a Guile REPL. For example:

```
$ guile
...
scheme@(guile-user)> (use-modules (inutil greetings))
scheme@(guile-user)> (list-greetings #:lang "es")
$1 = ("Hola" "Buenas" "Ey")
```


## Common Problems

### While compiling expression: no code for module (inutil greetings)

Make sure that Guile is installed in the guix profile you are using.
You can check by running:

```
$ guix package -I
```

If it is not listed, run

```
$ guix install guile
```

Then, run whatever guix suggests to update any necessary environment
variables. For example, today (2020-01-02), guix suggests to run the
following commands when using the Guix System:

```
$ GUIX_PROFILE="/home/YOUR_USER_NAME/.guix-profile"
$ . "$GUIX_PROFILE/etc/profile"
```

After this, you should be able to use `guile-inutil` modules in the
REPL. But I noticed that this only sets variables for the current
terminal you are using. If you switch to another terminal and start
guile, you get the same `no code for module (inutil greetings)` kind
of message. However, when you log out and log in again, you should
have no problem using the modules in any REPL in any terminal.

This environment variable dance is unfortunate, and I hope it changes
in the future.



[1]: https://www.gnu.org/software/guile/
[2]: https://guix.gnu.org/
[3]: https://gitlab.com/alekzikon/guix-channel-x