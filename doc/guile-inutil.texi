\input texinfo


@c HEADER
@setfilename guile-inutil.info
@settitle Useless Guile Library
@documentlanguage en
@documentencoding UTF-8
@afourpaper
@c END HEADER



@c TABLE OF CONTENTS
@iftex
@contents
@end iftex
@c END TABLE OF CONTENTS



@c MASTER MENU
@ifnottex
@node Top
@top Contents Overview

This is the documentation for the @code{guile-inutil} library, an
example library I use to learn how to package
@url{https://www.gnu.org/software/guile/, Guile} libraries for the
@url{https://guix.gnu.org/, Guix System}.
@end ifnottex


@menu
Chapters

* Introduction::
  What is Inutil, Guile, and Guix.
* Installation::
  Installing @code{guile-inutil} on the Guix System.
* Usage::
  Get started using the library.

API Reference

* inutil geometry::
  Operate on lengths, areas, and volumes.
* inutil greetings::
  Go crazy with greetings from all around the world.

Appendices

* Some Appendix::
* Another Appendix::

Indices

* Concept Index::
* Data Type Index::
* Procedure Index::

@end menu
@c END MASTER MENU



@c CHAPTERS
@node Introduction
@chapter Introduction

Content goes here...


@node Installation
@chapter Installation

Content goes here...


@node Usage
@chapter Usage

Content goes here...
@c END CHAPTERS



@c API
@node inutil geometry
@chapter (inutil geometry)

Content goes here...


@node inutil greetings
@chapter (inutil greetings)

Content goes here...
@c END API



@c APPENDICES
@node Some Appendix
@appendix Some Appendix

Content goes here...



@node Another Appendix
@appendix Another Appendix

Content goes here...
@c END APPENDICES



@c INDICES
@node Concept Index
@unnumbered Concept Index

@printindex cp

@node Data Type Index
@unnumbered Data Type Index

@printindex tp

@node Procedure Index
@unnumbered Function Index

@printindex fn
@c END INDICES



@bye
